package skijanje.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import skijanje.model.Skok;
import skijanje.service.SkokService;
import skijanje.service.TakmicarService;
import skijanje.web.dto.SkokDTO;

@Component
public class SkokDTOToSkok implements Converter<SkokDTO, Skok>{

	@Autowired
	private SkokService skokService;
	
	@Autowired
	private TakmicarService takmicarService;
		
	
	@Override
	public Skok convert(SkokDTO dto) {
		Skok skok;
		
		if(dto.getId()==null) {
			skok= new Skok();
		} else {
			skok= skokService.findOne(dto.getId());
		}
		
		if(skok !=null) {
			skok.setDaljina(dto.getDaljina());
			skok.setOcenaSudija(dto.getOcenaSudija());
			skok.setPoeniZaDaljinu(dto.getPoeniZaDaljinu());
			skok.setZbirPoena(dto.getZbirPoena());
			skok.setTakmicar(takmicarService.findOne(dto.getTakmicar().getId()));
		}
		return skok;
	}

}
