package skijanje.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import skijanje.model.Takmicar;
import skijanje.web.dto.TakmicarDTO;

@Component
public class TakmicarToTakmicarDTO implements Converter<Takmicar, TakmicarDTO>{

	@Autowired
	private SkakaonicaToSkakaonicaDTO skakaonicaToSkakaonicaDTO;
	
	@Override
	public TakmicarDTO convert(Takmicar takmicar) {
		
		TakmicarDTO dto = new TakmicarDTO();
		
		dto.setId(takmicar.getId());
		dto.setDrzava(takmicar.getDrzava());
		dto.seteMail(takmicar.geteMail());
		dto.setImePrezime(takmicar.getImePrezime());
		dto.setVisina(takmicar.getVisina());
		dto.setSkakaonica(skakaonicaToSkakaonicaDTO.convert(takmicar.getSkakaonica()));
		dto.setDatumRodjenja(takmicar.getDatumRodjenja().toString());
		
		return dto;
	}
	
	public List<TakmicarDTO> convert(List<Takmicar> takmicari) {
		List<TakmicarDTO> dto = new ArrayList<>();
		
		for (Takmicar takmicar : takmicari) {
			dto.add(convert(takmicar));
			}
		return dto;
	}

}
