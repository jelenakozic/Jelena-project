package skijanje.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import skijanje.model.Skok;
import skijanje.web.dto.SkokDTO;

@Component
public class SkokToSkokDTO implements Converter<Skok,SkokDTO> {

	@Autowired
	private TakmicarToTakmicarDTO takmicarToTakmicarDTO;
	
	@Override
	public SkokDTO convert(Skok skok) {
	
		SkokDTO dto = new SkokDTO();
		dto.setId(skok.getId());
		dto.setDaljina(skok.getDaljina());
		dto.setOcenaSudija(skok.getOcenaSudija());
		dto.setPoeniZaDaljinu(skok.getPoeniZaDaljinu());
		dto.setZbirPoena(skok.getZbirPoena());
		
		dto.setTakmicar(takmicarToTakmicarDTO.convert(skok.getTakmicar()));
		
		return dto;
	}
	public List<SkokDTO> convert(List<Skok> skokovi) {
		List<SkokDTO> dto = new ArrayList<>();
		
		for (Skok skok : skokovi) {
			dto.add(convert(skok));
			}
		return dto;
	}

}
