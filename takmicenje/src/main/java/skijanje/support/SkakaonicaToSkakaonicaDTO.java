package skijanje.support;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import skijanje.model.Skakaonica;
import skijanje.model.Takmicar;
import skijanje.web.dto.SkakaonicaDTO;

@Component
public class SkakaonicaToSkakaonicaDTO implements Converter<Skakaonica, SkakaonicaDTO>{

	
	
	@Override
	public SkakaonicaDTO convert(Skakaonica skakaonica) {
		SkakaonicaDTO dto = new SkakaonicaDTO();
		dto.setId(skakaonica.getId());
		dto.setNaziv(skakaonica.getNaziv());
		dto.setK(skakaonica.getK());
		dto.setD(skakaonica.getD());
		
		return dto;
	}
	public List<SkakaonicaDTO> convert(List<Skakaonica> skakaonice){
		List<SkakaonicaDTO> dto = new ArrayList<>();
		
		for (Skakaonica skakaonica : skakaonice) {
			dto.add(convert(skakaonica));
			
		}
		return dto;
		
	}
	}


