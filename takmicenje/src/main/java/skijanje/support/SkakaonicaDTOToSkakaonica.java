package skijanje.support;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import skijanje.model.Skakaonica;
import skijanje.model.Takmicar;
import skijanje.service.SkakaonicaService;
import skijanje.service.TakmicarService;
import skijanje.web.dto.SkakaonicaDTO;
import skijanje.web.dto.TakmicarDTO;

@Component
public class SkakaonicaDTOToSkakaonica implements Converter<SkakaonicaDTO, Skakaonica>{

	@Autowired
	private SkakaonicaService skakaonicaService;
	
	
	
	@Override
	public Skakaonica convert(SkakaonicaDTO dto) {
		Skakaonica skakaonica;
		
		if(dto.getId() == null) {
			skakaonica = new Skakaonica();
		} else {
			skakaonica = skakaonicaService.findOne(dto.getId());
		}
		if(skakaonica != null) {
			skakaonica.setNaziv(dto.getNaziv());
			skakaonica.setD(dto.getD());
			skakaonica.setK(dto.getK());
		
		 
		}
		return skakaonica;
	}
	

}
