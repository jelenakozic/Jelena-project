package skijanje.service;

import java.util.List;

import skijanje.model.Skok;
import skijanje.web.dto.SkokDTO;

public interface SkokService {

	List<Skok> findAll();
	
	Skok findOne(Long id);
	
	Skok update(Skok skok);
	
	List<Skok> findSkokoviByTakmicarId(Long id);
	
	Skok save(SkokDTO skokDTO);
	
}
