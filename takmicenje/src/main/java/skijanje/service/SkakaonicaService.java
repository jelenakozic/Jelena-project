package skijanje.service;

import java.util.List;

import skijanje.model.Skakaonica;

public interface SkakaonicaService {
	
	
	List<Skakaonica> findAll();
	
	Skakaonica findOne(Long id);
	
	Skakaonica update(Skakaonica skakaonica);
	
	

}
