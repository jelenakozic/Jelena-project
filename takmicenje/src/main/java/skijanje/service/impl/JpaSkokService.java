package skijanje.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import skijanje.model.Skok;
import skijanje.model.Takmicar;
import skijanje.repository.SkokRepository;
import skijanje.repository.TakmicarRepository;
import skijanje.service.SkokService;
import skijanje.support.SkokDTOToSkok;
import skijanje.web.dto.SkokDTO;


@Service
public class JpaSkokService implements SkokService{

	@Autowired
	private SkokRepository skokRepository;
	
	@Autowired
	private TakmicarRepository takmicarRepository;
	
	@Autowired
	private SkokDTOToSkok skokDTOToskok;
	
	
	@Override
	public List<Skok> findAll() {
		return skokRepository.findAll();
	}

	@Override
	public Skok findOne(Long id) {
		return skokRepository.findOneById(id);
	}

	@Override
	public Skok update(Skok skok) {
	return skokRepository.save(skok);
	}

	@Override
	public List<Skok> findSkokoviByTakmicarId(Long id) {
		return skokRepository.findByTakmicarId(id);
	}

	@Override
	public Skok save(SkokDTO skokDTO) {
		
		double maxOcena = 0;
		for(double trenutnaOcena: skokDTO.getPojedinacneOceneSudija()) {
			if(trenutnaOcena > maxOcena) {
				maxOcena = trenutnaOcena;	
			}
		}
		System.out.println("max ocena: " + maxOcena);

		double minOcena = 20;
		for(double trenutnaOcena: skokDTO.getPojedinacneOceneSudija()) {
			if(trenutnaOcena < minOcena) {
				minOcena = trenutnaOcena;	
			}
		}
		System.out.println("min ocena: " + minOcena);
		
		List<Double> listaPotrebnihVrednost = new ArrayList<Double>();
		for(double trenutnaOcena: skokDTO.getPojedinacneOceneSudija()) {
			if(trenutnaOcena != minOcena && trenutnaOcena != maxOcena) {
				listaPotrebnihVrednost.add(trenutnaOcena);
			}
		}
		
		
		double ukupnaSuma = 0;
		for (Double ocena : listaPotrebnihVrednost) {
			ukupnaSuma += ocena;
		}
		
		double prosecnaOcena = ukupnaSuma / listaPotrebnihVrednost.size();

		skokDTO.setOcenaSudija(prosecnaOcena);
		
		Long takmicarId=skokDTO.getTakmicar().getId();
		Takmicar takmicar = takmicarRepository.findOneById(takmicarId);
		
		double k= takmicar.getSkakaonica().getK();
		double d= takmicar.getSkakaonica().getD();
		
		double daljina = skokDTO.getDaljina();
		
		double poeni= 0;
		if (daljina > k) {
			 poeni = k + (daljina-k)*d;
		}
		if (daljina < k) {
			 poeni = k - (k-daljina)*d;
		}
		
		
		skokDTO.setPoeniZaDaljinu(poeni);
		

		skokDTO.setZbirPoena(poeni + prosecnaOcena);
		
		Skok skok = skokDTOToskok.convert(skokDTO);
		
		return skokRepository.save(skok);
	}


}
