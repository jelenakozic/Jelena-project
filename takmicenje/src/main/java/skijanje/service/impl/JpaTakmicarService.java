package skijanje.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import skijanje.model.Takmicar;
import skijanje.repository.TakmicarRepository;
import skijanje.service.TakmicarService;

@Service
public class JpaTakmicarService implements TakmicarService{

	@Autowired
	private TakmicarRepository takmicarRepository;
	
	@Override
	public List<Takmicar> findAll() {
		return takmicarRepository.findAll();
	}

	@Override
	public Takmicar findOne(Long id) {
		return takmicarRepository.findOneById(id);
	}

	@Override
	public Takmicar save(Takmicar takmicar) {
		return takmicarRepository.save(takmicar);
	}

	@Override
	public Takmicar update(Takmicar takmicar) {
		return takmicarRepository.save(takmicar);
	}

	@Override
	public Takmicar delete(Long id) {
		Optional<Takmicar> takmicar = takmicarRepository.findById(id);
		if(takmicar.isPresent()) {
			takmicarRepository.deleteById(id);
			return takmicar.get();
		}
		return null;
	}

	@Override
	public Page<Takmicar> pretraga(Long skakaonicaId,String imePrezime,
			int pageNum) {
		
		return takmicarRepository.pretraga(skakaonicaId,  imePrezime, PageRequest.of(pageNum, 5));
	}
	
   
	@Override
	public List<Takmicar> find(List<Long> ids) {
		return takmicarRepository.findByIdIn(ids);
	}

}
