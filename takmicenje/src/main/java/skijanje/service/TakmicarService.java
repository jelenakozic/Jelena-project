package skijanje.service;

import java.util.List;

import org.springframework.data.domain.Page;



import skijanje.model.Takmicar;



public interface TakmicarService {
	
	List<Takmicar> findAll();
	 
	Takmicar findOne(Long id);
	 
	Takmicar save(Takmicar takmicar);
	 
	Takmicar update(Takmicar takmicar);
	 
	Takmicar delete(Long id);
	
	 List<Takmicar> find(List<Long> ids);
	 
	 Page<Takmicar> pretraga(Long skakaonicaId, String imePrezime, int pageNum);
}
