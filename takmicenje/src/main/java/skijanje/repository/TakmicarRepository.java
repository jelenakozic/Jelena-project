package skijanje.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;



import skijanje.model.Takmicar;

public interface TakmicarRepository extends JpaRepository<Takmicar, Long> { 
	
	Takmicar findOneById(Long id);
	
	List<Takmicar> findByIdIn(List<Long> ids);
	
	@Query("SELECT t FROM Takmicar t WHERE "
    		+ "(:skakaonicaId IS NULL OR t.skakaonica.id = :skakaonicaId) AND "
    		+ "(:imePrezime IS NULL OR t.imePrezime like %:imePrezime% )")
    Page<Takmicar> pretraga(@Param("skakaonicaId") Long skakaonicaId,  @Param("imePrezime") String imePrezime, Pageable pageable);


}
