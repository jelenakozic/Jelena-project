package skijanje.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import skijanje.model.Skok;

public interface SkokRepository extends JpaRepository<Skok, Long>{

	Skok findOneById(Long id);
	
	List<Skok> findByTakmicarId(Long id);
}
