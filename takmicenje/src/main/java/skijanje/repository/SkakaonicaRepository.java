package skijanje.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import skijanje.model.Skakaonica;

public interface SkakaonicaRepository extends JpaRepository<Skakaonica, Long>{

	Skakaonica findOneById(Long id);
}
