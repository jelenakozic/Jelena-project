package skijanje.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table
public class Skok {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column
	private double daljina;
	
	@Column
	private double poeniZaDaljinu;
	
	@Column
	private double ocenaSudija;
	
	
	@Column
	private double zbirPoena;
	
	@ManyToOne(fetch= FetchType.LAZY, cascade= CascadeType.ALL)
	private Takmicar takmicar;

	public Skok() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public double getDaljina() {
		return daljina;
	}

	public void setDaljina(double daljina) {
		this.daljina = daljina;
	}

	public double getPoeniZaDaljinu() {
		return poeniZaDaljinu;
	}

	public void setPoeniZaDaljinu(double poeniZaDaljinu) {
		this.poeniZaDaljinu = poeniZaDaljinu;
	}

	public double getOcenaSudija() {
		return ocenaSudija;
	}

	public void setOcenaSudija(double ocenaSudija) {
		this.ocenaSudija = ocenaSudija;
	}

	public double getZbirPoena() {
		return zbirPoena;
	}

	public void setZbirPoena(double zbirPoena) {
		this.zbirPoena = zbirPoena;
	}

	public Takmicar getTakmicar() {
		return takmicar;
	}

	public void setTakmicar(Takmicar takmicar) {
		this.takmicar = takmicar;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Skok other = (Skok) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Skok [id=" + id + ", daljina=" + daljina + ", poeniZaDaljinu=" + poeniZaDaljinu + ", ocenaSudija="
				+ ocenaSudija + ", zbirPoena=" + zbirPoena + ", takmicar=" + takmicar + "]";
	}

	
	
	
	
	
}
