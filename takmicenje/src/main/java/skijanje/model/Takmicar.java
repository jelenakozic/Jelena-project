package skijanje.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;



@Entity
@Table
public class Takmicar {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false)
	private String imePrezime;
	
	@Column
	private String drzava;
	
	@Column(nullable = false)
	private double visina;
	
	@Column(unique = true)
	private String eMail;
	
	@Column
	private Date datumRodjenja;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Skakaonica skakaonica;
	
	@OneToMany(mappedBy = "takmicar", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<Skok> skokovi = new ArrayList<Skok>();

	public Takmicar() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getImePrezime() {
		return imePrezime;
	}

	public void setImePrezime(String imePrezime) {
		this.imePrezime = imePrezime;
	}

	public String getDrzava() {
		return drzava;
	}

	public void setDrzava(String drzava) {
		this.drzava = drzava;
	}

	public double getVisina() {
		return visina;
	}

	public void setVisina(double visina) {
		this.visina = visina;
	}

	public String geteMail() {
		return eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public Date getDatumRodjenja() {
		return datumRodjenja;
	}

	public void setDatumRodjenja(Date datumRodjenja) {
		this.datumRodjenja = datumRodjenja;
	}

	public Skakaonica getSkakaonica() {
		return skakaonica;
	}

	public void setSkakaonica(Skakaonica skakaonica) {
		this.skakaonica = skakaonica;
	}

	public List<Skok> getSkokovi() {
		return skokovi;
	}

	public void setSkokovi(List<Skok> skokovi) {
		this.skokovi = skokovi;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Takmicar other = (Takmicar) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Takmicar [id=" + id + ", imePrezime=" + imePrezime + ", drzava=" + drzava + ", visina=" + visina
				+ ", eMail=" + eMail + ", datumRodjenja=" + datumRodjenja + ", skakaonica=" + skakaonica + ", skokovi="
				+ skokovi + "]";
	}

	
}
