import React from "react";
import ReactDOM from "react-dom";
import { Route, Link, HashRouter as Router, Switch, Redirect } from "react-router-dom";
import Home from "./components/Home";
import AddTakmicar from "./components/takmicari/AddTakmicar";
import EditTakmicar from "./components/takmicari/EditTakmicar";
import Takmicari from "./components/takmicari/Takmicari";
import AddSkok from "./components/takmicari/AddSkok";
import NotFound from "./components/NotFound";
import Login from './components/authentication/Login';
import Skokovi from "./components/skokovi/Skokovi";

import { Navbar, Nav, Container, Button } from "react-bootstrap";
import {logout} from './services/auth';

class App extends React.Component {
    render() {
  
      let token = window.localStorage.getItem("token");
  
      if(token){
        return (
          <div>
            <Router>
              <Navbar bg="dark" variant="dark" expand fixed="top">
                <Navbar.Brand>
                  <Link to="/">JWD</Link>
                </Navbar.Brand>
                <Nav className="mr-auto">
                  <Nav.Link as={Link} to="/takmicari">
                    Takmicari
                  </Nav.Link>
                  <Nav.Link as={Link} to="/skokovi">
                    Skokovi
                  </Nav.Link>
                  </Nav>

                  
                <Button onClick={()=>logout()}>Logout</Button>
              </Navbar>
    
              <br />
              <br />
              <br />
    
              <Container>
                <Switch>
                  <Route exact path="/" component={Home} />
                  <Route exact path="/login" render={()=><Redirect to="/" />} />
                  <Route exact path="/takmicari" component={Takmicari} />
                  <Route exact path="/takmicari/add" component={AddTakmicar} />
                  <Route exact path="/skokovi/add" component={AddSkok} />
                  <Route exact path="/takmicari/edit/:id" component={EditTakmicar} />
                  <Route exact path="/skokovi" component={Skokovi}/>
                  
                  <Route component={NotFound} />
                </Switch>
              </Container>
            </Router>
          </div>
        );
      }else{
  
        return (
          <Container>
            <Router>
              <Switch>
                <Route exact path="/login" component={Login} />
                <Route render={()=><Redirect to="/login" />} />
              </Switch>
            </Router>
          </Container>
        );
  
      }
  
      
    }
  }
  
  ReactDOM.render(<App />, document.querySelector("#root"));
  