import React from 'react';
import JelenaAxios from '../../apis/JelenaAxios';

// import './../../index.css';

import {Button, Table, Form} from 'react-bootstrap';


class Skokovi extends React.Component {

    constructor(props) {
        super(props);
    
       
        this.state = { skokovi: [], takmicari: []};
      }
    
      
      componentDidMount() {
         this.getSkokovi();
         this.getTakmicari();
    }

    async getSkokovi(){
        try{
            let result = await JelenaAxios.get("/skokovi");
            let skokovi = result.data;
            this.setState({skokovi: skokovi});
        }catch(error){
            console.log(error);
            alert("Couldn't fetch skokovi");
        }
        
    }

    goToSkok() {
      this.props.history.push("/skokovi/add");
    }
    async getTakmicari() {
        try {
            let result = await JelenaAxios.get("/takmicari");
            let takmicari = result.data;
            this.setState({takmicari:takmicari});
        } catch (error) {
            console.log(error);
            alert("Couldn't fetch takmicari");
        }
    }
    render() {
        return (
          <div>
            <h1>Skokovi</h1>
            <br></br>
            <br></br>

            <Button onClick={() => this.goToSkok()}>
            Add
          </Button>

          <br></br>
          <br></br>
            <Table striped id="movies-table">
                <thead className="thead-dark">
                  <tr>
                    <th>Takmicar</th>
                    <th>Daljina</th>
                    <th>Zbir poena</th>
                    <th></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.skokovi.map((skok) => {
                    return (
                      <tr key={skok.id}>
                        <td>{skok.takmicar.imePrezime}</td>
                        <td>{skok.daljina}</td>
                        <td>{skok.zbirPoena}</td>
                        <td></td>
                      </tr>
                    );
                  })}
                </tbody>
              </Table>
            </div>
           
          
        );
      }
    }
    
    export default Skokovi;
    