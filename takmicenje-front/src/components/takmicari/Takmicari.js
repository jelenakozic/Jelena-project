import React from 'react';
import JelenaAxios from '../../apis/JelenaAxios';

// import './../../index.css';

import {Button, Table, Form} from 'react-bootstrap';


class Takmicari extends React.Component {

    constructor(props) {
        super(props);
    
        let params = {
         
          skakaonicaId: "",
          imePrezime: "",
        };
    
        this.state = { takmicari: [], skakaonice: [], skokovi:[], params: params, pageNum: 0, totalPages: 1};
      }
    
      componentDidMount() {
        this.getTakmicari();
        this.getSkakaonice();
        this.getSkokovi();
        
      }
    
      async getTakmicari() {
        try {
    
          let config = {params:{}};
          
          if(this.state.params.skakaonicaId != ""){
            config.params.skakaonicaId = this.state.params.skakaonicaId;
          }
          if(this.state.params.imePrezime != ""){
            config.params.imePrezime = this.state.params.imePrezime;
          }
    
          config.params.pageNum = this.state.pageNum;
    
          let result = await JelenaAxios.get("/takmicari", config);
          this.setState({ takmicari: result.data, totalPages: result.headers["total-pages"]});
        } catch (error) {
          console.log(error);
        }
      }
    
      async getSkakaonice(){
        try{
            let result = await JelenaAxios.get("/skakaonice");
            let skakaonice = result.data;
            this.setState({skakaonice: skakaonice});
        }catch(error){
            console.log(error);
            alert("Couldn't fetch skakaonice");
        }
        
    }
    async getSkokovi(){
      try{
          let result = await JelenaAxios.get("/skokovi");
          let skokovi = result.data;
          this.setState({skokovi: skokovi});
      }catch(error){
          console.log(error);
          alert("Couldn't fetch skokovi");
      }
  }
   
    
    deleteFromState(takmicarId) {
      var takmicari = this.state.takmicari;
      takmicari.forEach((element, index) => {
          if (element.id === takmicarId) {
            takmicari.splice(index, 1);
              this.setState({takmicari: takmicari});
          }
      });
  }
  
  delete(takmicarId) {
      JelenaAxios.delete('/takmicari/' + takmicarId)
      .then(res => {
          // handle success
          console.log(res);
          alert('Takmicar was deleted successfully!');
          // this.deleteFromState(zadatakId); // ili refresh page-a window.location.reload();
          this.getTakmicari();
      })
      .catch(error => {
          // handle error
          console.log(error);
          alert('Error occured please try again!');
       });
  }
      goToAdd() {
        this.props.history.push("/takmicari/add");
      }
      
    
      valueInputChanged(e){
        let control = e.target;
    
        let name = control.name;
        let value = control.value;
    
        let params = this.state.params;
        params[name] = value;
    
        this.setState({params: params});
      }
    
      doSearch(e){
        e.preventDefault();
    
        this.setState({pageNum: 0}, ()=>{this.getTakmicari();});
      }
    
      changePage(direction){
        let pageNum = this.state.pageNum;
        pageNum = pageNum + direction;
    
        this.setState({pageNum: pageNum}, ()=>{this.getTakmicari()});
      }

      goToEdit(takmicarId) {
        this.props.history.push('/takmicari/edit/'+ takmicarId); 
    }
  
    
      render() {
        return (
          <div>
            <h1>Takmicari</h1>
    
            <Form>
              <Form.Group>
                <Form.Label>Skakaonica</Form.Label>
                <Form.Control onChange={(event)=>this.valueInputChanged(event)} name="skakaonicaId" as="select">
                  <option value=""></option>
                  {
                    this.state.skakaonice.map((skakaonica) => {
                      return <option key={skakaonica.id} value={skakaonica.id}>{skakaonica.naziv}</option>
                    })
                  }
                </Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label>Ime i prezime takmicara</Form.Label>
                <Form.Control onChange={(e)=>{this.valueInputChanged(e);}} name="imePrezime"></Form.Control>
              </Form.Group>
              
              <Button style={{marginBottom:"10px"}} onClick={(e)=>this.doSearch(e)}>Search</Button>
            </Form>
    
            <div>
              <br></br>
              <br></br>
              <Button onClick={() => this.goToAdd()}>
                Add
              </Button>
              <br></br>
              <br></br>
    
              <Button disabled={this.state.pageNum==0} onClick={()=>this.changePage(-1)}>Previous</Button>
              <Button disabled={this.state.pageNum == this.state.totalPages - 1} onClick={()=>this.changePage(1)}>Next</Button>
              <Table striped id="movies-table">
                <thead className="thead-dark">
                  <tr>
                    <th>Ime i prezime</th>
                    <th>Drzava</th>
                    <th>Visina</th>
                    <th>Datum rodjenja</th>
                    <th>Skakaonica</th>
                    <th></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.takmicari.map((takmicar) => {
                    return (
                      <tr key={takmicar.id}>
                        <td>{takmicar.imePrezime}</td>
                        <td>{takmicar.drzava}</td>
                        <td>{takmicar.visina}</td>
                        <td>{takmicar.datumRodjenja}</td>
                        <td>{takmicar.skakaonica.naziv}</td>
                        
                        <td><Button  onClick={() => this.goToEdit(takmicar.id)}>Edit</Button></td>
                        <td><Button  onClick={() => this.delete(takmicar.id)}>Delete</Button></td>
                        {/* <td><Button  onClick={() => this.changeStanje(zadatak.id)}>Promena stanja</Button></td>
                       */}
                      </tr>
                    );
                  })}
                </tbody>
              </Table>
            </div>
          </div>
        );
      }
    }
    
    export default Takmicari;
    