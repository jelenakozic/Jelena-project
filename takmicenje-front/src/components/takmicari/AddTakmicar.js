import React from 'react';
import JelenaAxios from '../../apis/JelenaAxios';

import {Form, Button} from 'react-bootstrap';

class AddTakmicar extends React.Component {

    constructor(props){
        super(props);

        let takmicar = {
            imePrezime: "",
            drzava: "",
            visina: 0,
            eMail: "",
            datumRodjenja: "",
            skakaonica: null
        }

        this.state = {takmicar: takmicar, skakaonice: [],  };
    }

    componentDidMount(){
        this.getSkakaonice();
        
        console.log("test2");
    }

    // TODO: Dobaviti filmove
    async getSkakaonice(){
        try{
            let result = await JelenaAxios.get("/skakaonice");
            let skakaonice = result.data;
            this.setState({skakaonice: skakaonice});
            console.log("test1");
        }catch(error){
            console.log(error);
            alert("Couldn't fetch skakaonice");
        }
    }

    

    async create(e){
        e.preventDefault();

        try{

            
           
            let takmicar = this.state.takmicar;
            let takmicarDTO = {

                datumRodjenja: takmicar.datumRodjenja,
                visina: takmicar.visina,
                imePrezime: takmicar.imePrezime,
                drzava: takmicar.drzava,
                eMail: takmicar.eMail,
                skakaonica: takmicar.skakaonica
            }

            let response = await JelenaAxios.post("/takmicari", takmicarDTO);
            this.props.history.push("/takmicari");
        }catch(error){
            alert("Greska");
        }
    }

    valueInputChanged(e) {
        let input = e.target;
    
        let name = input.name;
        let value = input.value;
    
        let takmicar = this.state.takmicar;
        takmicar[name] = value;
    
        this.setState({ takmicar: takmicar });
      }

    // TODO: Rukovati prihvatom vrednosti na promenu
    skakaonicaSelectionChanged(e){
        // console.log(e);

        let skakaonicaId = e.target.value;
        let skakaonica = this.state.skakaonice.find((skakaonica) => skakaonica.id == skakaonicaId);

        let takmicar = this.state.takmicar;
        takmicar.skakaonica = skakaonica;

        this.setState({takmicar: takmicar});
    }

    
    // TODO: Omogućiti odabir filma za projekciju
    render(){
        return (
            <div>
                <h1>Add Takmicar</h1>

                <Form>
                    <Form.Group>
                        <Form.Label htmlFor="imePrezime">Ime i prezime</Form.Label>
                        <Form.Control id="imePrezime" name="imePrezime" onChange={(e)=>this.valueInputChanged(e)}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label htmlFor="drzava">Drzava</Form.Label>
                        <Form.Control id="drzava" name="drzava" onChange={(e)=>this.valueInputChanged(e)}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label id="visina">Visina</Form.Label>
                        <Form.Control type="number" id="visina" name="visina" onChange={(e)=>this.valueInputChanged(e)}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label htmlFor="datumRodjenja">Datum rodjenja</Form.Label>
                        <Form.Control   id="datumRodjenja" name="datumRodjenja" onChange={(e)=>this.valueInputChanged(e)}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label htmlFor="eMail">Email</Form.Label>
                        <Form.Control   id="eMail" name="eMail" onChange={(e)=>this.valueInputChanged(e)}/>
                    </Form.Group>
                    
                    <Form.Group>
                        <Form.Label htmlFor="skakaonica">Skakaonica</Form.Label >


                    </Form.Group>
                    
                    <Form.Control as="select" id="skakaonica" onChange={event => this.skakaonicaSelectionChanged(event)}>
                        <option></option>
                        {
                            this.state.skakaonice.map((skakaonica) => {
                                return (
                                    <option key={skakaonica.id} value={skakaonica.id}>{skakaonica.naziv}</option>
                                )
                            })
                        }
                    </Form.Control>
                            <br></br>
                    <Button variant="success" onClick={(event)=>{this.create(event);}}>Add</Button>
                </Form>
            </div>
        )
    }
}

export default AddTakmicar;