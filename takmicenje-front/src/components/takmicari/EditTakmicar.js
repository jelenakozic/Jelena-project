import React from 'react';
import JelenaAxios from '../../apis/JelenaAxios';

import {Form, Button} from 'react-bootstrap';

class EditTakmicar extends React.Component {

    constructor(props) {
        super(props);

        let takmicar = {
            imePrezime: "",
            drzava: "",
            visina: 0,
            eMail: "",
            datumRodjenja: "",
            skakaonica: null
        }

        this.state = {takmicar: takmicar, skakaonice: []};
    }

    componentDidMount() {
       this.getTakmicarById(this.props.match.params.id);
       this.getSkakaonice();
       
    }

    getTakmicarById(takmicarId) {
        JelenaAxios.get('/takmicari/' + takmicarId)
        .then(res => {
            // handle success
            console.log('ZADATAK:')
            console.log(res);
            this.setState({takmicarId: res.data.id, skakaonica:res.data.skakaonica, imePrezime:res.data.imePrezime,  drzava:res.data.drzava, visina:res.data.visina, eMail:res.data.eMail, datumRodjenja:res.data.datumRodjenja});
        })
        .catch(error => {
            // handle error
            console.log(error);
            alert('Error occured please try again!');
         });
    }

    async getSkakaonice(){
        try{
            let result = await JelenaAxios.get("/skakaonice");
            let skakaonice = result.data;
            this.setState({skakaonice: skakaonice});
        }catch(error){
            console.log(error);
            alert("Couldn't fetch skakaonice");
        }
    }
    
    // valueInputChanged = event => {
    //         console.log(event.target.value);
    
    //        const { name, value } = event.target;
    //        console.log(name + ", " + value);
    
    //      this.setState((state, props) => ({
                
    //              name: value
    //     }));
    // }

    onImeChange = event => {
        console.log(event.target.value);

        const { name, value } = event.target;
        console.log(name + ", " + value);

        this.setState((state, props) => ({
            
            imePrezime: value
        }));
    }
    onDrzavaChange = event => {
        console.log(event.target.value);

        const { name, value } = event.target;
        console.log(name + ", " + value);

        this.setState((state, props) => ({
            
            drzava: value
        }));
    }

    onVisinaChange = event => {
        console.log(event.target.value);

        const { name, value } = event.target;
        console.log(name + ", " + value);

        this.setState((state, props) => ({
           
            visina: value
        }));
    }
    onSkakaonicaChange = event => {
        console.log('Vrednost selecta')
        console.log(event.target.value);
        const skakaonicaId = event.target.value;
        let odabranaSkakaonica = this.state.skakaonice.find((skakaonica) => skakaonica.id == skakaonicaId);

        const { name, value } = event.target;
        console.log(name + ", " + value);

        this.setState((state, props) => ({
            skakaonica: odabranaSkakaonica
        }));
        console.log('na kraju state sprinta');
        console.log(this.state)
    }
    onDatumChange = event => {
        console.log(event.target.value);

        const { name, value } = event.target;
        console.log(name + ", " + value);

        this.setState((state, props) => ({
            
            datumRodjenja: value
        }));
    }
    edit() {
        var params = {
            'id': this.state.takmicarId,
            'skakaonica': this.state.skakaonica,
            'datumRodjenja': this.state.datumRodjenja,
            'visina': this.state.visina,
            'imePrezime': this.state.imePrezime,
            'drzava': this.state.drzava,
            'eMail' : this.state.eMail
        };

        JelenaAxios.put('/takmicari/' + this.state.takmicarId, params)
        .then(res => {
            // handle success
            console.log(res);
            alert('Takmicar was edited successfully!');
            this.props.history.push('/takmicari');
        })
        .catch(error => {
            // handle error
            console.log(error);
            alert('Error occured please try again!');
         });
    }

    render() {
        return (
            <div>
                <h1>Edit takmicar</h1>
                <Form>
                    <Form.Group>
                    
                    <Form.Label htmlFor="imePrezime">Ime</Form.Label>
                    <Form.Control id="imePrezime" name="imePrezime" value={this.state.imePrezime} onChange={(e) => this.onImeChange(e)}/>
                    <Form.Label htmlFor="drzava">Drzava</Form.Label>
                    <Form.Control id="drzava" name="drzava" value={this.state.drzava} onChange={(e) => this.onDrzavaChange(e)}/>
                    <Form.Label htmlFor="visina">Visina</Form.Label>
                    <Form.Control id="visina" name="visina" type="number" value={this.state.visina} onChange={(e) => this.onVisinaChange(e)}/>
                    <Form.Label htmlFor="datumRodjenja">Datum rodjenja</Form.Label>
                    <Form.Control id="datumRodjenja" name="datumRodjenja"  value={this.state.datumRodjenja} onChange={(e) => this.onDatumChange(e)}/>
                    <Form.Label htmlFor="skakaonica">Skakaonica</Form.Label>
                    <Form.Control as="select" id="skakaonica" onChange={(e) => this.onSkakaonicaChange(e)}>
                        <option></option>
                        {
                            this.state.skakaonice.map((skakaonica) => {
                                return (
                                    <option key={skakaonica.id} value={skakaonica.id}>{skakaonica.naziv}</option>
                                )
                            })
                        }
                    </Form.Control><br/>
                    <Button className="button button-navy" onClick={() => this.edit()}>Edit</Button>
                    </Form.Group>
                    </Form>
                
            </div>
        );
    }
}

export default EditTakmicar;