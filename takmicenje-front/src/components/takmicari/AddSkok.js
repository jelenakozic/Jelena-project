import React from 'react';
import JelenaAxios from '../../apis/JelenaAxios';

import {Form, Button} from 'react-bootstrap';

class AddSkok extends React.Component {

    constructor(props){
        super(props);
        let skok = {
            daljina: 0,
            poeniZaDaljinu: 0,
            ocenaSudija: 0,
            zbirPoena: 0,
            takmicar: null,
            pojedinacneOceneSudija: []
        }

        this.state = {skok: skok, takmicari: [],  pojedinacneOceneSudija:[], ocenaSudija1:0, ocenaSudija2:0, ocenaSudija3:0, ocenaSudija4:0, ocenaSudija5:0 };
    }

    componentDidMount(){
        this.getTakmicari();
        
        console.log("test2");
    }

    
    async getTakmicari(){
        try{
            let result = await JelenaAxios.get("/takmicari");
            let takmicari = result.data;
            this.setState({takmicari: takmicari});
            console.log("test1");
        }catch(error){
            console.log(error);
            alert("Couldn't fetch takmicari");
        }
    }

    

    async create(e){
        e.preventDefault();

       
        try{
            this.state.skok.pojedinacneOceneSudija = [this.state.ocenaSudija1, 
                this.state.ocenaSudija2, this.state.ocenaSudija3, this.state.ocenaSudija4, this.state.ocenaSudija5]
           
            let skok = this.state.skok;
            let skokDTO = {

                daljina: skok.daljina,
                poeniZaDaljinu: skok.poeniZaDaljinu,
                ocenaSudija: skok.ocenaSudija,
                pojedinacneOceneSudija: this.state.skok.pojedinacneOceneSudija,
                zbirPoena: skok.zbirPoena,
                takmicar: skok.takmicar
            }

            let response = await JelenaAxios.post("/skokovi", skokDTO);
            alert("Uspesno odradjeno, proverite na backendu");
            this.props.history.push("/takmicari");
        }catch(error){
            alert("Greska");
        }
    }

    ocena1Changed(e) {
        let input = e.target;
    
        let name = input.name;
        let value = input.value;
    
        let ocenaSudija1 = this.state.ocenaSudija1;
        ocenaSudija1 = value;
    
        this.setState({ ocenaSudija1: ocenaSudija1 });
      }
      ocena2Changed(e) {
        let input = e.target;
    
        let name = input.name;
        let value = input.value;
    
        let ocenaSudija2 = this.state.ocenaSudija2;
        ocenaSudija2 = value;
    
        this.setState({ ocenaSudija2: ocenaSudija2 });
      }
      ocena3Changed(e) {
        let input = e.target;
    
        let name = input.name;
        let value = input.value;
    
        let ocenaSudija3 = this.state.ocenaSudija3;
        ocenaSudija3 = value;
    
        this.setState({ ocenaSudija3: ocenaSudija3 });
      }
      ocena4Changed(e) {
        let input = e.target;
    
        let name = input.name;
        let value = input.value;
    
        let ocenaSudija4 = this.state.ocenaSudija4;
        ocenaSudija4 = value;
    
        this.setState({ ocenaSudija4: ocenaSudija4 });
      }
      ocena5Changed(e) {
        let input = e.target;
    
        let name = input.name;
        let value = input.value;
    
        let ocenaSudija5 = this.state.ocenaSudija5;
        ocenaSudija5 = value;
    
        this.setState({ ocenaSudija5: ocenaSudija5 });
      }

    valueInputChanged(e) {
        let input = e.target;
    
        let name = input.name;
        let value = input.value;
    
        let skok = this.state.skok;
        skok[name] = value;
    
        this.setState({ skok: skok });
      }

    // TODO: Rukovati prihvatom vrednosti na promenu
    takmicarSelectionChanged(e){
        // console.log(e);

        let takmicarId = e.target.value;
        let takmicar = this.state.takmicari.find((takmicar) => takmicar.id == takmicarId);

        let skok = this.state.skok;
        skok.takmicar = takmicar;

        this.setState({skok: skok});
    }

    
    // TODO: Omogućiti odabir filma za projekciju
    render(){
        return (
            <div>
                <h1>Add Skok</h1>

                <Form>
                    <Form.Group>
                        <Form.Label htmlFor="daljina">Daljina</Form.Label>
                        <Form.Control id="daljina" type="number" name="daljina" onChange={(e)=>this.valueInputChanged(e)}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label htmlFor="poeniZaDaljinu">Poeni za daljinu</Form.Label>
                        <Form.Control id="poeniZaDaljinu" type="number" name="poeniZaDaljinu" onChange={(e)=>this.valueInputChanged(e)}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label htmlFor="ocenaSudija1">Ocena sudije 1</Form.Label>
                        <Form.Control   id="ocenaSudija1" type="number" name="ocenaSudija1" onChange={(e)=>this.ocena1Changed(e)}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label htmlFor="ocenaSudija2">Ocena sudije 2</Form.Label>
                        <Form.Control   id="ocenaSudija2" type="number" name="ocenaSudija2" onChange={(e)=>this.ocena2Changed(e)}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label htmlFor="ocenaSudija3">Ocena sudije 3</Form.Label>
                        <Form.Control   id="ocenaSudija3" type="number" name="ocenaSudija3" onChange={(e)=>this.ocena3Changed(e)}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label htmlFor="ocenaSudija4">Ocena sudije 4</Form.Label>
                        <Form.Control   id="ocenaSudija4" type="number" name="ocenaSudija4" onChange={(e)=>this.ocena4Changed(e)}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label htmlFor="ocenaSudija5">Ocena sudije 5</Form.Label>
                        <Form.Control   id="ocenaSudija5" type="number" name="ocenaSudija5" onChange={(e)=>this.ocena5Changed(e)}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label id="ocenaSudija">Ocena Sudija</Form.Label>
                        <Form.Control type="number" id="ocenaSudija" name="ocenaSudija" onChange={(e)=>this.valueInputChanged(e)}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label htmlFor="zbirPoena">Zbir poena</Form.Label>
                        <Form.Control   id="zbirPoena" type = "number" name="zbirPoena" onChange={(e)=>this.valueInputChanged(e)}/>
                    </Form.Group>
                   
                    
                    <Form.Group>
                        <Form.Label htmlFor="takmicar">Takmicar</Form.Label >


                    </Form.Group>
                    
                    <Form.Control as="select" id="takmicar" onChange={event => this.takmicarSelectionChanged(event)}>
                        <option></option>
                        {
                            this.state.takmicari.map((takmicar) => {
                                return (
                                    <option key={takmicar.id} value={takmicar.id}>{takmicar.imePrezime}</option>
                                )
                            })
                        }
                    </Form.Control>
                            <br></br>
                    <Button variant="success" onClick={(event)=>{this.create(event);}}>Add</Button>
                </Form>
            </div>
        )
    }
}

export default AddSkok;
