import React from "react";

import {login} from "../../services/auth";
import {Button, Table, Form} from 'react-bootstrap';

class Login extends React.Component {
  constructor() {
    super();

    this.state = { username: "", password: "" };
  }

  valueInputChange(event) {
    let control = event.target;

    let name = control.name;
    let value = control.value;

    let change = {};
    change[name] = value;
    this.setState(change);
  }


  doLogin(e){
    e.preventDefault();

    login(this.state.username, this.state.password);
  }

  // TODO: Ulepšati: - centrirati, udaljiti od vrha, staviti jumbotron
  // TODO: Završiti implementaciju
  render() {
    return (
      <div>
        <h1>Welcome!</h1>
        <Form>
              <Form.Group>
              <Form.Label>Username</Form.Label>
              
       
            
            <Form.Control
              onChange={(e) => {
                this.valueInputChange(e);
              }}
              name="username"
              
            ></Form.Control>
            </Form.Group>
            <Form.Group>
            <Form.Label>Password</Form.Label>
            
            
          
          
            <Form.Control
              onChange={(e) => {
                this.valueInputChange(e);
              }}
              name="password"
              type="password"
            ></Form.Control>
          </Form.Group>
          <Button onClick={(e) => {this.doLogin(e)}}>Log in</Button>
        </Form>
      </div>
    );
  }
}

export default Login;
